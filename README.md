# Teste Gympass

## kart-racing-service


Projeto gerado com [Angular CLI](https://github.com/angular/angular-cli) version 1.3.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


## Docker
./gradlew build -x test docker

docker run -d -p 9000:9000 com.gympass.interview-test/kart-racing-service




