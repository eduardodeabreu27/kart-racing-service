package com.gympass.interviewtest.kartracing.util;

import java.io.Serializable;

public class HttpResponse extends HeaderHttpResponse implements Serializable {

    private Object Object;

    public java.lang.Object getObject() {
        return Object;
    }

    public void setObject(java.lang.Object object) {
        Object = object;
    }

}
