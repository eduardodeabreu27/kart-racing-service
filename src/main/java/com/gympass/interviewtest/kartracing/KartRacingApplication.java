package com.gympass.interviewtest.kartracing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KartRacingApplication {

	public static void main(String[] args) {
		SpringApplication.run(KartRacingApplication.class, args);
	}

}
