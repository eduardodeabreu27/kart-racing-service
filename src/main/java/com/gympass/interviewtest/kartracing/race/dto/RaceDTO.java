package com.gympass.interviewtest.kartracing.race.dto;

import java.util.List;

public class RaceDTO {


    String totalTime;
    String bestTurnTime;
    String nameRacingDriverWinner;
    List<RacingDriverDTO> listResultRace;

    public String getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(String totalTime) {
        this.totalTime = totalTime;
    }

    public String getBestTurnTime() {
        return bestTurnTime;
    }

    public void setBestTurnTime(String bestTurnTime) {
        this.bestTurnTime = bestTurnTime;
    }

    public String getNameRacingDriverWinner() {
        return nameRacingDriverWinner;
    }

    public void setNameRacingDriverWinner(String nameRacingDriverWinner) {
        this.nameRacingDriverWinner = nameRacingDriverWinner;
    }

    public List<RacingDriverDTO> getListResultRace() {
        return listResultRace;
    }

    public void setListResultRace(List<RacingDriverDTO> listResultRace) {
        this.listResultRace = listResultRace;
    }
}
