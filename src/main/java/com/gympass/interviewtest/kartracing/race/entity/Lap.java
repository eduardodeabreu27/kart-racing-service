package com.gympass.interviewtest.kartracing.race.entity;

import java.time.LocalTime;
import java.util.List;

public class Lap {

    private List<Position> positionList;
    private int turn;
    private LocalTime bestTime;
    private Double averageSpeed;

    public int getTurn() {
        return turn;
    }

    public void setTurn(int turn) {
        this.turn = turn;
    }

    public LocalTime getBestTime() {
        return bestTime;
    }

    public void setBestTime(LocalTime bestTime) {
        this.bestTime = bestTime;
    }

    public Double getAverageSpeed() {
        return averageSpeed;
    }

    public void setAverageSpeed(Double averageSpeed) {
        this.averageSpeed = averageSpeed;
    }

    public List<Position> getPositionList() {
        return positionList;
    }

    public void setPositionList(List<Position> positionList) {
        this.positionList = positionList;
    }
}
