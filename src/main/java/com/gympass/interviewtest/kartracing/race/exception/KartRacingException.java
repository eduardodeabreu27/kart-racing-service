package com.gympass.interviewtest.kartracing.race.exception;

import java.text.MessageFormat;

public class KartRacingException extends RuntimeException {

    private static final long serialVersionUID = 3422494428773141388L;

    protected Object[] params;

    public KartRacingException(final String mensagem) {
        super(mensagem);
    }

    public KartRacingException(final String mensagem, final Object[] params) {
        super(MessageFormat.format(mensagem, params));
    }

    public KartRacingException(final String mensagem, final Throwable cause) {
        super(mensagem, cause);
    }

    public KartRacingException(final String mensagem, final Object[] params, final Throwable cause) {
        super(MessageFormat.format(mensagem, params), cause);
    }

    protected KartRacingException() {
        super();
    }

    public KartRacingException(final Throwable cause) {
        super(cause);
    }

    public Object[] getParams() {
        return params;
    }

}
