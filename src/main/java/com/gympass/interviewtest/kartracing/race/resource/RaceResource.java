package com.gympass.interviewtest.kartracing.race.resource;


import com.gympass.interviewtest.kartracing.race.dto.RaceDTO;
import com.gympass.interviewtest.kartracing.race.entity.Race;
import com.gympass.interviewtest.kartracing.race.service.RaceService;
import com.gympass.interviewtest.kartracing.util.HTTPStatusEnum;
import com.gympass.interviewtest.kartracing.util.HttpResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.Serializable;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@RequestMapping(value = "/race")
public class RaceResource {

    @Autowired
    private RaceService raceService;

    @CrossOrigin
    @RequestMapping(value = "/result", method = GET)
    public Serializable result() {

        Serializable response = null;
        try {
            RaceDTO raceDTO = raceService.findResultRace();
            if(raceDTO != null){
                response = httpResponseOk(HTTPStatusEnum.STATUS_200.getDescription(), raceDTO);
            } else {
                response = httpResponseNotFound(HTTPStatusEnum.STATUS_404.getDescription(), raceDTO);
            }

        } catch (Exception e) {

            response = httpResponseInternalServerError(e.getMessage(), null);
        }
        return response;
    }

    /**
     *
     * @param message
     * @return
     */
    protected HttpResponse httpResponseOk(String message, Object object) {
        return this.createHttpResponse(HTTPStatusEnum.STATUS_200, message, object);
    }
    /**
     *
     * @param message
     * @return
     */
    protected HttpResponse httpResponseNotFound(String message, Object object) {
        return this.createHttpResponse(HTTPStatusEnum.STATUS_404, message, object);
    }


    /**
     *
     * @param message
     * @return
     */
    protected HttpResponse httpResponseInternalServerError(String message, Object object) {
        return this.createHttpResponse(HTTPStatusEnum.STATUS_501, message, object);
    }

    private HttpResponse createHttpResponse(HTTPStatusEnum status, String message, Object object) {
        HttpResponse httpResponse = new HttpResponse();
        httpResponse.setResponseCode(status.getStatusCode());
        httpResponse.setResponseDescription(message);
        httpResponse.setObject(object);
        return httpResponse;
    }

}
