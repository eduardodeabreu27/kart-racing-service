package com.gympass.interviewtest.kartracing.race.service;

import com.gympass.interviewtest.kartracing.race.dto.RaceDTO;
import com.gympass.interviewtest.kartracing.race.dto.RacingDriverDTO;
import com.gympass.interviewtest.kartracing.race.entity.Lap;
import com.gympass.interviewtest.kartracing.race.entity.Position;
import com.gympass.interviewtest.kartracing.race.entity.Race;
import com.gympass.interviewtest.kartracing.race.entity.RacingDriver;
import com.gympass.interviewtest.kartracing.race.exception.ServiceException;
import com.gympass.interviewtest.kartracing.race.repository.RaceRepository;
import com.gympass.interviewtest.kartracing.race.wrapper.LapWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class RaceService {

    @Autowired
    private RaceRepository repository;
    private Map<Integer, RacingDriver> racingDriverHashMap = new HashMap<>();
    private List<LapWrapper> lapWrapperList;
    Map<Integer, List<LapWrapper>> resultLapWrapperMap;

    /**
     * Ler resultado do log, tratar as regras e montar os dados de retorno
     *
     * @return Race
     */
    public RaceDTO findResultRace(){


        lapWrapperList = readLog();
        List<Lap> lapList = getOrderedLapList(lapWrapperList);
        Lap finalLap = lapList.get(lapList.size() - 1);
        setRacingDriverList(lapList);
        RacingDriver raceWinner = getWinnerRacingDriver(Integer.valueOf(finalLap.getPositionList().get(0).getRacingDriver().getCode()));


        Race race = getRace(lapList, raceWinner);
        RaceDTO raceDTO = getRaceDTO(race, lapList);

        return raceDTO;
    }

    public List<LapWrapper> readLog(){
        return repository.findResultRaceByLog();
    }


    /**
     * Ler resultado do log, tratar as regras e montar os dados de retorno
     *
     * @return Race
     */
    public Race getRace(List<Lap> lapList, RacingDriver raceWinner){

        Race race = new Race();
        race.setLapList(lapList);
        race.setRaceWinner(raceWinner);

        return race;
    }

    /**
     * Ordenar a lista de voltas
     * @return
     */
    public List<Lap> getOrderedLapList(List<LapWrapper> lapWrapperList){
        try {
            List<Lap> lapList = new ArrayList<>();
            resultLapWrapperMap = lapWrapperList.stream()
                    .collect(Collectors.groupingBy(LapWrapper::getTurn));
            for (Map.Entry<Integer,List<LapWrapper>> listLapWrapperEntry : resultLapWrapperMap.entrySet()) {
                List<LapWrapper> lapWrapperListToOrder = listLapWrapperEntry.getValue();
                Collections.sort(lapWrapperListToOrder);
                List<Position> positionList = new ArrayList<>();

                Lap lap = new Lap();
                int lapNumber = 1;
                for (LapWrapper lapWrapper : lapWrapperListToOrder){
                    Position position = new Position();
                    position.setId(lapNumber);
                    position.setArrivalTime(lapWrapper.getArrivalTime());
                    position.setRacingDriver(lapWrapper.getRacingDriver());
                    position.setTurnTime(lapWrapper.getTurnTime());
                    position.setAverageSpeed(lapWrapper.getAverageSpeed());
                    positionList.add(position);
                    lapNumber++;
                }
                lap.setPositionList(positionList);
                lapList.add(lap);
            }
            return lapList;
        } catch (Exception ex){
            throw new ServiceException("Ocorreu um problema ao obter o resultado da corrida");
        }

    }

    public void setRacingDriverList(List<Lap> lapList){
        Lap firstLap = lapList.get(0);
        List<Position> positionList = firstLap.getPositionList();
        for (Position position : positionList){
            racingDriverHashMap.put(Integer.valueOf(position.getRacingDriver().getCode()), position.getRacingDriver());
        }
    }

    public RacingDriver getWinnerRacingDriver(Integer codeRacingDriverWinner){
        RacingDriver racingDriver = null;
        for (Integer codeRacingDriver : racingDriverHashMap.keySet()) {
            if(codeRacingDriver == codeRacingDriverWinner){
                racingDriver = racingDriverHashMap.get(codeRacingDriver);
            }
        }
        return racingDriver;
    }

    private RaceDTO getRaceDTO(Race race, List<Lap> lapList){

        RaceDTO raceDTO = new RaceDTO();
        raceDTO.setNameRacingDriverWinner(race.getRaceWinner().getCode() + " - " + race.getRaceWinner().getName());
        raceDTO.setBestTurnTime(String.valueOf(getBestTurnTime(race)));
        List<RacingDriverDTO> racingDriverDTOList = new ArrayList<>();
        List<Position> positionList = lapList.get(0).getPositionList();

        int countToGetWinner = 0;
        for (Position position : lapList.get(lapList.size()-1).getPositionList()){
            String codeWinner = "";
            if(countToGetWinner == 0){
                codeWinner = String.valueOf(position.getRacingDriver().getCode());
            }
            RacingDriverDTO racingDriverDTO = new RacingDriverDTO();
            racingDriverDTO.setPosition(String.valueOf(position.getId()));
            racingDriverDTO.setRacingDriverCode(String.valueOf(position.getRacingDriver().getCode()));
            racingDriverDTO.setRacingDriverName(String.valueOf(position.getRacingDriver().getName().trim()));
            int completedTurn = getNumberOfTurnCompleted(position.getRacingDriver().getCode(), race);
            racingDriverDTO.setQuantityTurnCompleted(String.valueOf(completedTurn));
            racingDriverDTO.setBestTurnTime(String.valueOf(getBestTurnTimeFromRacingDriver(position.getRacingDriver().getCode())));
            racingDriverDTO.setTotalTime(String.valueOf(getTotalTimeFromRacingDriver(String.valueOf(position.getRacingDriver().getCode()))));
            racingDriverDTO.setArrivalTime(String.valueOf(position.getArrivalTime()));
            racingDriverDTO.setTimeToWinner(String.valueOf(getTimeDiffFromRacingDriverWinner(race.getRaceWinner().getCode(), String.valueOf(position.getRacingDriver().getCode()))));
            racingDriverDTO.setAverageSpeed(String.valueOf(getAverageSpeed(position.getRacingDriver().getCode())));
            racingDriverDTOList.add(racingDriverDTO);

        }
        racingDriverDTOList = verifyRacingDriverNotCompleted(racingDriverDTOList);
        raceDTO.setListResultRace(racingDriverDTOList);
        return raceDTO;
    }

    private LocalTime getBestTurnTime(Race race){
        LocalTime bestTurnTime = null;
        List<Lap> lapList = race.getLapList();
        int cont = 0;
        for(Lap lap : lapList){
            for (Position position : lap.getPositionList()){
                if(cont == 0){
                    bestTurnTime = position.getTurnTime();
                } else {
                    bestTurnTime = position.getTurnTime().isBefore(bestTurnTime) ? position.getTurnTime() : bestTurnTime;
                }
                cont++;
            }
        }
        return  bestTurnTime;
    }

    private int getNumberOfTurnCompleted(String codeRacingDriver, Race race){
        RacingDriver racingDriver = null;
        int countLap = 0;
        List<Lap> lapList = race.getLapList();
        for (Lap lap : lapList){
            for (Position position : lap.getPositionList()){
                if(position.getRacingDriver().getCode().equals(codeRacingDriver)){
                    countLap++;
                }
            }
        }
        return countLap;
    }

    /**
     * Best Turn Time
     * @param
     * @param codeRacingDriver
     * @return
     */
    private LocalTime getBestTurnTimeFromRacingDriver(String codeRacingDriver){

        try {
            List<LapWrapper> lapListFromRacingDriver = lapWrapperList.stream().filter((s -> s.getRacingDriver().getCode().equals(codeRacingDriver))).collect(Collectors.toList());
            lapListFromRacingDriver.sort(Comparator.comparing(LapWrapper::getTurnTime));
            return lapListFromRacingDriver.get(0).getTurnTime();
        } catch (Exception ex){
            throw new ServiceException("Ocorreu um problema ao calcular o tempo total do piloto");
        }
    }


    private LocalTime getTotalTimeFromRacingDriver(String codeRacingDriver){

        try{
            List<LapWrapper> lapListFromRacingDriver = lapWrapperList.stream().filter((s -> s.getRacingDriver().getCode().equals(codeRacingDriver))).collect(Collectors.toList());
            lapListFromRacingDriver.sort(Comparator.comparing(LapWrapper::getTurn));
            return lapListFromRacingDriver.get(lapListFromRacingDriver.size()-1).getArrivalTime().minusNanos(lapListFromRacingDriver.get(0).getArrivalTime().toNanoOfDay());
        } catch (Exception ex){
            throw new ServiceException("Ocorreu um problema ao calcular o tempo total do piloto");
        }


    }

    private LocalTime getTimeDiffFromRacingDriverWinner(String codeRacingDriverWinner, String codeRacingDriver){
        try {
            List<LapWrapper> lapListFromRacingDriver = lapWrapperList.stream().filter((s -> s.getRacingDriver().getCode().equals(codeRacingDriver))).collect(Collectors.toList());
            List<LapWrapper> lapListFromRacingDriverWinner = lapWrapperList.stream().filter((s -> s.getRacingDriver().getCode().equals(codeRacingDriverWinner))).collect(Collectors.toList());
            lapListFromRacingDriverWinner.sort(Comparator.comparing(LapWrapper::getTurn));
            lapListFromRacingDriver.sort(Comparator.comparing(LapWrapper::getTurn));
            return lapListFromRacingDriver.get(lapListFromRacingDriver.size()-1).getArrivalTime().minusNanos(lapListFromRacingDriverWinner.get(lapListFromRacingDriverWinner.size()-1).getArrivalTime().toNanoOfDay());
        } catch (Exception ex){
            throw new ServiceException("Ocorreu um problema ao calcular o tempo de chegada após o vencedor");
        }


    }

    private Double getAverageSpeed(String codeRacingDriver){
        try{
            List<LapWrapper> lapWrapperListByRacingDriver = lapWrapperList.stream().filter((s -> s.getRacingDriver().getCode().equals(codeRacingDriver))).collect(Collectors.toList());
            Double averageSpeed = 0.0;
            int quantityTurn = 0;
            for (LapWrapper lapWrapper : lapWrapperListByRacingDriver){
                averageSpeed = averageSpeed + lapWrapper.getAverageSpeed();
                quantityTurn++;
            }
            return averageSpeed/quantityTurn;
        } catch (Exception ex){
            throw new ServiceException("Ocorreu um problema ao calcular velocidade média");
        }

    }



    private List<RacingDriverDTO> verifyRacingDriverNotCompleted(List<RacingDriverDTO> racingDriverDTOList){
        boolean notFound = false;
        ArrayList<Integer> keys = new ArrayList<Integer>(resultLapWrapperMap.keySet());
        for(int i=keys.size()-1; i>=0;i--){
            System.out.println(resultLapWrapperMap.get(keys.get(i)));
            List<LapWrapper> listPositionInLap = resultLapWrapperMap.get(keys.get(i));
            if(racingDriverHashMap.size() != listPositionInLap.size() ){
                LapWrapper lap = listPositionInLap.get(i);
                for (Integer codeRacingDriver : racingDriverHashMap.keySet()) {
                    RacingDriver racingDriver = racingDriverHashMap.get(codeRacingDriver);
                    notFound = true;
                    for (LapWrapper lapWrapper : listPositionInLap){
                        if(lapWrapper.getRacingDriver().getCode().equalsIgnoreCase(racingDriver.getCode())){
                            notFound = false;
                        }
                    }
                    if(notFound){
                        for(int j=listPositionInLap.size()-1; j>=0;j--){
                            LapWrapper lapWrapper = listPositionInLap.get(j);
                            List<LapWrapper> listPositionInLastLap = resultLapWrapperMap.get(keys.get(j-1));
                            for(LapWrapper lapp : listPositionInLastLap){
                                if(lapp.getRacingDriver().getCode().equalsIgnoreCase(racingDriver.getCode())){
                                    RacingDriverDTO racingDriverDTO = new RacingDriverDTO();
                                    racingDriverDTO.setRacingDriverCode(racingDriver.getCode());
                                    racingDriverDTO.setRacingDriverName(racingDriver.getName());
                                    racingDriverDTO.setPosition(String.valueOf(listPositionInLastLap.size()));
                                    racingDriverDTO.setQuantityTurnCompleted(String.valueOf(j));
                                    racingDriverDTO.setBestTurnTime(String.valueOf(getBestTurnTimeFromRacingDriver(racingDriver.getCode())));
                                    racingDriverDTO.setTotalTime(String.valueOf(getTotalTimeFromRacingDriver(String.valueOf(racingDriver.getCode()))));
                                    racingDriverDTO.setAverageSpeed(String.valueOf(getAverageSpeed(racingDriver.getCode())));
                                    racingDriverDTOList.add(racingDriverDTO);
                                    return racingDriverDTOList;
                                }
                            }
                        }
                    }
                }
            }
        }
        return racingDriverDTOList;
    }
}
