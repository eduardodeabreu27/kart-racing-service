package com.gympass.interviewtest.kartracing.race.dto;

import java.time.LocalTime;

public class RacingDriverDTO {

    String position;
    String racingDriverCode;
    String racingDriverName;
    String quantityTurnCompleted;
    String totalTime;
    String bestTurnTime;
    String averageSpeed;
    String timeToWinner;
    String arrivalTime;

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getRacingDriverCode() {
        return racingDriverCode;
    }

    public void setRacingDriverCode(String racingDriverCode) {
        this.racingDriverCode = racingDriverCode;
    }

    public String getRacingDriverName() {
        return racingDriverName;
    }

    public void setRacingDriverName(String racingDriverName) {
        this.racingDriverName = racingDriverName;
    }

    public String getQuantityTurnCompleted() {
        return quantityTurnCompleted;
    }

    public void setQuantityTurnCompleted(String quantityTurnCompleted) {
        this.quantityTurnCompleted = quantityTurnCompleted;
    }

    public String getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(String totalTime) {
        this.totalTime = totalTime;
    }

    public String getBestTurnTime() {
        return bestTurnTime;
    }

    public void setBestTurnTime(String bestTurnTime) {
        this.bestTurnTime = bestTurnTime;
    }

    public String getAverageSpeed() {
        return averageSpeed;
    }

    public void setAverageSpeed(String averageSpeed) {
        this.averageSpeed = averageSpeed;
    }

    public String getTimeToWinner() {
        return timeToWinner;
    }

    public void setTimeToWinner(String timeToWinner) {
        this.timeToWinner = timeToWinner;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }
}
