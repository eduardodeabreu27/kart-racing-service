package com.gympass.interviewtest.kartracing.race.repository;


import com.gympass.interviewtest.kartracing.race.entity.RacingDriver;
import com.gympass.interviewtest.kartracing.race.exception.ServiceException;
import com.gympass.interviewtest.kartracing.race.wrapper.LapWrapper;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.NumberFormat;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Repository
public class RaceRepository {

    public List<LapWrapper> findResultRaceByLog(){
        Path path;
        List<LapWrapper> lapWrapperList =  new ArrayList<>();
        try {
            path = Paths.get("src/main/resources/result_race.txt");
        } catch (Exception ex){
            throw new ServiceException("Ocorreu um problema ao ler o log");
        }
        try{
            List<String> lapsRace = Files.readAllLines(path);

            int i = 0;
            for (String lap : lapsRace) {
                if(i > 0){

                    LapWrapper lapWrapper = new LapWrapper();
                    String hora = lap.substring(0,12);
                    String piloto = lap.substring(18,58);
                    String numeroVolta = lap.substring(58,66);
                    String tempo = lap.substring(67,75);
                    String velocidadeMedia = lap.substring(99,lap.length());
                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss.SSS");
                    LocalTime arrivalTime = LocalTime.parse(hora, formatter);
                    LocalTime turnTime = LocalTime.parse("00:0"+tempo, formatter);
                    String[] split = piloto.split(" – ");
                    RacingDriver racingDriver = new RacingDriver();
                    racingDriver.setCode(split[0]);
                    racingDriver.setName(split[1]);
                    Number number = NumberFormat.getInstance().parse(velocidadeMedia);
                    double d = number.doubleValue();
                    lapWrapper.setRacingDriver(racingDriver);
                    lapWrapper.setArrivalTime(arrivalTime);
                    lapWrapper.setTurn(Integer.valueOf(numeroVolta.trim()));
                    lapWrapper.setAverageSpeed(d);
                    lapWrapper.setTurnTime(turnTime);
                    lapWrapperList.add(lapWrapper);

                }
                i++;

            }
        } catch (Exception ex){
            System.out.println(ex);
        }

        return lapWrapperList;
    }


}
