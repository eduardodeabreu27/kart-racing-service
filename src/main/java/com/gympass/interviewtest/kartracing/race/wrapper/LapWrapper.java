package com.gympass.interviewtest.kartracing.race.wrapper;

import com.gympass.interviewtest.kartracing.race.entity.RacingDriver;

import java.time.LocalTime;

public class LapWrapper implements Comparable<LapWrapper> {

    private RacingDriver racingDriver;
    private int turn;
    private LocalTime turnTime;
    private Double averageSpeed;
    private LocalTime arrivalTime;


    public RacingDriver getRacingDriver() {
        return racingDriver;
    }

    public void setRacingDriver(RacingDriver racingDriver) {
        this.racingDriver = racingDriver;
    }

    public int getTurn() {
        return turn;
    }

    public void setTurn(int turn) {
        this.turn = turn;
    }

    public LocalTime getTurnTime() {
        return turnTime;
    }

    public void setTurnTime(LocalTime turnTime) {
        this.turnTime = turnTime;
    }

    public Double getAverageSpeed() {
        return averageSpeed;
    }

    public void setAverageSpeed(Double averageSpeed) {
        this.averageSpeed = averageSpeed;
    }

    public LocalTime getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(LocalTime arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    @Override
    public int compareTo(LapWrapper o) {
        if (getArrivalTime() == null || o.getArrivalTime() == null) {
            return 0;
        }
        return getArrivalTime().compareTo(o.getArrivalTime());
    }
}
