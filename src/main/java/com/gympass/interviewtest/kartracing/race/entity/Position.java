package com.gympass.interviewtest.kartracing.race.entity;

import java.time.LocalTime;

public class Position {

    private int id;
    private RacingDriver racingDriver;
    private LocalTime arrivalTime;
    private LocalTime turnTime;
    private Double averageSpeed;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalTime getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(LocalTime arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public RacingDriver getRacingDriver() {
        return racingDriver;
    }

    public void setRacingDriver(RacingDriver racingDriver) {
        this.racingDriver = racingDriver;
    }

    public LocalTime getTurnTime() {
        return turnTime;
    }

    public void setTurnTime(LocalTime turnTime) {
        this.turnTime = turnTime;
    }

    public Double getAverageSpeed() {
        return averageSpeed;
    }

    public void setAverageSpeed(Double averageSpeed) {
        this.averageSpeed = averageSpeed;
    }
}
