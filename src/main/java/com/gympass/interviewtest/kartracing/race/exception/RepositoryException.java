package com.gympass.interviewtest.kartracing.race.exception;

public class RepositoryException extends KartRacingException {

    private static final long serialVersionUID = -8454540961921832433L;

    public RepositoryException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public RepositoryException(final String message, final Object[] params, final Throwable cause) {
        super(message, params, cause);
    }

    public RepositoryException(final String message) {
        super(message);
    }

}
