package com.gympass.interviewtest.kartracing.race.entity;

import com.gympass.interviewtest.kartracing.race.wrapper.LapWrapper;

import java.time.LocalTime;
import java.util.List;

public class Race {

    private RacingDriver raceWinner;
    private List<Lap> lapList;
    private LocalTime totalTime;


    public RacingDriver getRaceWinner() {
        return raceWinner;
    }

    public void setRaceWinner(RacingDriver raceWinner) {
        this.raceWinner = raceWinner;
    }

    public List<Lap> getLapList() {
        return lapList;
    }

    public void setLapList(List<Lap> lapList) {
        this.lapList = lapList;
    }

    public LocalTime getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(LocalTime totalTime) {
        this.totalTime = totalTime;
    }
}
