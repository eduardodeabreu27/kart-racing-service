package com.gympass.interviewtest.kartracing;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gympass.interviewtest.kartracing.race.dto.RaceDTO;
import com.gympass.interviewtest.kartracing.race.entity.RacingDriver;
import com.gympass.interviewtest.kartracing.race.repository.RaceRepository;
import com.gympass.interviewtest.kartracing.race.resource.RaceResource;
import com.gympass.interviewtest.kartracing.race.service.RaceService;
import com.gympass.interviewtest.kartracing.race.wrapper.LapWrapper;
import com.gympass.interviewtest.kartracing.util.HttpResponse;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;

import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@AutoConfigureMockMvc
public class KartRacingApplicationTests {

	final String BASE_PATH = "http://localhost:8080";

	@Autowired
	private RaceService raceService;

	@Autowired
	private RaceRepository raceRepository;

	@Test
	public void testBestTurnTime(){
		RaceDTO raceDTO = raceService.findResultRace();
		Assert.assertEquals(raceDTO.getBestTurnTime(), "00:01:02.769");
	}

	@Test
	public void testReadLog() throws Exception {
		Assert.assertNotNull(raceRepository.findResultRaceByLog());
	}

	@Test
	public void testNumberOfRacers() throws Exception {
		RaceDTO raceDTO = raceService.findResultRace();
		Assert.assertTrue(raceDTO.getListResultRace().size()  == 6 );
	}

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void testCallRestApi() throws Exception {
		mockMvc.perform(get("/race/result")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}

}
